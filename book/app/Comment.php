<?php

namespace book;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    /**
     * ОТНОШЕНИЯ: Пользователь
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('book\User');
    }

    /**
     * ОТНОШЕНИЯ: Сообщение
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function message()
    {
        return $this->belongsTo('book\User');
    }


    public function childComments()
    {
        $childComments = Comment::where('parent_comment_id',$this->id)->get();
        return $childComments;

    }
}
