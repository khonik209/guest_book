<?php

namespace book;

use Illuminate\Database\Eloquent\Model;

class Message extends Model
{

    /**
     * ОТНОШЕНИЯ: Комментарии
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function comments()
    {
        return $this->hasMany('book\Comment');
    }

    /**
     * ОТНОШЕНИЯ: Пользовател
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('book\User');
    }
}
