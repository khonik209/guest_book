<?php

namespace book\Mail;

use book\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class AccessData extends Mailable
{
    use Queueable, SerializesModels;

    public $user;
    public $password;

    /**
     * AccessData constructor.
     * @param User $user
     * @param $password
     */
    public function __construct(User $user,$password)
    {
        $this->user = $user;
        $this->password = $password;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.welcome');
    }
}
