<?php

namespace book\Http\Controllers;

use book\Comment;
use book\Message;
use book\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\Facades\Image;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $messages = Message::orderBy('created_at', 'desc')->paginate(25);
        return view('home', compact('messages'));
    }

    /**
     * Создание нового сообщения
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function newMessage(Request $request)
    {
        $user = Auth::user();

        $message = new Message;
        $message->user_id = $user->id;
        $message->text = $request->text;
        if ($request->hasFile('image')) {

            $file = $request->file('image');
            $extension = $file->clientExtension();
            $filename = time() . '.' . $extension;

            $image = Image::make($request->file('image'))->orientate();

            $imageWidth = $image->width();

            if ($imageWidth > 500) {
                $image->widen(500)->save('storage/' . $filename);
            } else {
                $image->save('storage/reports/' . $filename);
            }

            $message->image = $filename;
        }
        $message->save();

        return back()->with('success', 'Сохранено');

    }

    /**
     * Добавление нового комментария
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function newComment(Request $request)
    {
        $validation = Validator::make($request->all(), [
            'text' => 'required|string|max:1000',
            //'image' => 'required|string|email|max:255|unique:users',

        ]);
        if ($validation->fails()) {
            return back()->withInput()->with('error', 'Ошибка при добавлении сообщения');
        }
        $user = Auth::user();

        /* Если мы отвечаем на основую запись в книге, крепим к сообщению */
        if($request->message_id) {
            $message = Message::findOrFail($request->message_id);
            $message->is_answered = 1;
            $message->save();
        }

        $comment = new Comment;
        $comment->user_id = $user->id;
        $comment->text = $request->text;
        if($request->message_id) {
            $comment->message_id = $message->id;
        }
        /* Если мы отвечаем на комментарий - крепим к комментарию */
        if($request->comment_id)
        {
            $comment->parent_comment_id = $request->comment_id;
        }
        $comment->save();

        return back()->with('success', 'Сохранено');

    }

    /**
     * Страница редактирования сообщения
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function messageEditPage($id)
    {
        $message = Message::findOrFail($id);
        $user = Auth::user();
        if ($message->user_id != $user->id) {
            return back()->with('error', 'В доступе отказано');
        }
        return view('messages.edit', [
            'message' => $message
        ]);
    }

    /**
     * Сохранить отредактированное сообщение
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function messageEdit(Request $request)
    {
        $validation = Validator::make($request->all(), [
            'text' => 'required|string|max:1000',
            'image' => 'image|max:100',

        ]);
        if ($validation->fails()) {
            return back()->withInput()->with('error', 'Ошибка при редактировнии сообщения');
        }
        $message = Message::findOrFail($request->message_id);
        $message->text = $request->text;
        if ($request->hasFile('image')) {

            $file = $request->file('image');
            $extension = $file->clientExtension();
            $filename = time() . '.' . $extension;

            $image = Image::make($request->file('image'))->orientate();

            $imageWidth = $image->width();

            if ($imageWidth > 500) {
                $image->widen(500)->save('storage/' . $filename);
            } else {
                $image->save('storage/reports/big_' . $filename);
            }

            $message->image = $filename;
        } else {
            $message->image = null;
        }
        $message->save();

        return redirect('/home')->with('success','Изменено');
    }

    public function validateMessageForm(Request $request)
    {
        $validation = Validator::make($request->all(), [
            'text' => 'required|string|max:1000',
            'image' => 'image|max:100',

        ]);
        if ($validation->fails()) {
            return 'Ошибка заполнения формы';
        }

        if($request->hasFile('image'))
        {
            $file = $request->file('image');
            $image = Image::make($file)->orientate();

            $imageWidth = $image->width();
            $imageHeight = $image->height();
            if($imageWidth<100 || $imageHeight<100){
                return "Слишком маленькое изображение";
            }
        }
        return 0;
    }
}
