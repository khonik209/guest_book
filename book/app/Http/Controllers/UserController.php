<?php

namespace book\Http\Controllers;

use book\Mail\AccessData;
use book\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{
    public function register(Request $request)
    {
       print 'ПЕРЕШЛИ НА СОЗДАНИЕ';
       exit;
        $user = new User;
        $user->name = $request->name;
        $user->email = $request->email;
        $user->birthday = $request->birthday;
        $user->sex = $request->sex;

        $password = str_random(8);
        $user->password = bcrypt($password);
        $user->save();

        Auth::login($user);
        try {
            Mail::to($user)->send(new AccessData($user, $password));
        } catch (\Exception $e) {
            Log::error('Ошибка при отправке доступов: ' . $e);
        }
        return redirect('/home');
    }

    public function login(Request $request)
    {
        if (Auth::attempt(['email' => $request->email, 'password' => $request->password]) || Auth::attempt(['name' => $request->email, 'password' => $request->password])) {
            // Аутентификация успешна
            return redirect('/home');
        } else {
            return back();
        }
    }

    public function validateRegisterForm(Request $request)
    {
        $checkName = User::where('name', $request->name)->first();
        if ($checkName) {
            return 'Пользователь с таким username уже есть';
        }
        $checkEmail = User::where('email', $request->email)->first();
        if ($checkEmail) {
            return 'Пользователь с таким email уже есть';
        }

        return 0;
    }
}
