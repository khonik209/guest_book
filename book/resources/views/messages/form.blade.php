<form action="{{url('/message/new')}}" method="post" enctype="multipart/form-data" class="form-validate">
    {{ csrf_field() }}
    <div class="form-group">
        <label for="text">Сообщение</label>
        <input type="text" name="text" class="form-control required" id="text" placeholder="Введите своё сообщение сюда">
    </div>
    <div class="form-group">
        <label for="image">Прикрепить изображение</label>
        <input type="file" id="image" name="image">
        <p class="help-block">Не более 100 кБ</p>
    </div>
    <button type="submit" class="btn btn-primary">Отправить</button>
</form>