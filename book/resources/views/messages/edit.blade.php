@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Редактирование сообщения</div>
                    <div class="panel-body">
                        @include('common.error')
                        @include('common.info')
                        @include('common.success')
                        <form action="{{url('/message/edit')}}" method="post" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <input type="hidden" name="message_id" value="{{$message->id}}">
                            <textarea name="text" class="form-control"
                                      placeholder="Введите новое сообщение">{{$message->text}}</textarea>
                            <hr>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="image">Изображение</label>
                                    <input type="file" name="image" id="image">
                                </div>
                                <button class="btn btn-primary" type="submit">Сохранить</button>
                            </div><!-- /.col-lg-6 -->
                            @if($message->image)
                                <div class="col-sm-6">
                                    <img src="/storage/{{$message->image}}" class="img img-responsive">
                                </div>
                            @endif
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection