<div class="panel panel-default">
    <div class="panel-body">
        <h3 class="panel-title">
           <strong> {{$message->user->name}}</strong>

            <small class="pull-right">{{\Carbon\Carbon::parse($message->created_at)->format('m.d.Y H:i')}}</small>

        </h3>
        @if($message->user_id == Auth::id() && !$message->is_answered )
            <a href="{{url('/message/'.$message->id.'/edit')}}" class="btn btn-info btn-xs pull-right"><span class="glyphicon glyphicon-pencil"></span></a>
        @endif
        @if($message->image)
            <div class="media">
                <a class="pull-left" href="#">
                    <img class="media-object img-responsive" src="/storage/{{$message->image}}" alt="...">
                </a>
                <div class="media-body">
                    {{$message->text}}
                </div>
            </div>
        @else
            {{$message->text}}
        @endif
        <hr>
        <div class="col-sm-offset-3 col-sm-9">
            @each('comments.comment', $message->comments->sortByDesc('created_at'), 'comment', 'comments.empty')

        </div>
        <div class="col-xs-12">
            <hr>
            @include('comments.form')
        </div>
    </div>
</div>