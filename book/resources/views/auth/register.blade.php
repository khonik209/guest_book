@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Register</div>

                    <div class="panel-body">
                        @include('common.error')
                        @include('common.info')
                        @include('common.success')
                        <form class="form-horizontal" method="POST" action="{{ route('register') }}" id="register-form">
                            {{ csrf_field() }}

                            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                <label for="name" class="col-md-4 control-label">Name</label>

                                <div class="col-md-6">
                                    <input id="name" type="text" class="form-control required" name="name"
                                           value="{{ old('name') }}" autofocus>

                                    @if ($errors->has('name'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                                <div class="col-md-6">
                                    <input id="email" type="email" class="form-control required" name="email"
                                           value="{{ old('email') }}" >

                                    @if ($errors->has('email'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="birthday" class="col-md-4 control-label">Birthday</label>
                                <div class="col-md-6">
                                    <input id="birthday" type="date" class="form-control required" name="birthday"
                                           value="{{ old('birthday') }}" >
                                </div>
                            </div>

                            <div class="col-md-offset-4 col-md-6">
                            <div class="radio-inline">
                                <label>
                                    <input type="radio" name="sex" id="male" value="male" checked>
                                    Мужской
                                </label>
                            </div>
                            <div class="radio-inline">
                                <label>
                                    <input type="radio" name="sex" id="female" value="female">
                                    Женский
                                </label>
                            </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-6 col-md-offset-4">
                                    <button type="submit" class="btn btn-primary">
                                        Register
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
