<p><strong>{{$comment->user->name}}</strong>
    <small class="pull-right">{{\Carbon\Carbon::parse($comment->created_at)->format('m.d.Y H:i')}}</small>
</p>
<p>{{$comment->text}}</p>
@include('comments.form')
<div class="col-sm-offset-3 col-sm-9">
    @each('comments.comment', $comment->childComments()->sortByDesc('created_at'), 'comment')
</div>
<hr>