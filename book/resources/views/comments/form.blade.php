<form action="{{url('/comment/new')}}" method="post" class="comment-validate">
    {{ csrf_field() }}
    @if(isset($message))
        <input type="hidden" name="message_id" value="{{$message->id}}">
    @endif
    @if(isset($comment))
        <input type="hidden" name="comment_id" value="{{$comment->id}}">
    @endif
    <div class="col-xs-12">
        <div class="input-group">
            <input type="text" name="text" class="form-control required">
            <span class="input-group-btn">
        <button class="btn btn-default" type="submit">Ответить</button>
      </span>
        </div><!-- /input-group -->
    </div><!-- /.col-lg-6 -->
</form>