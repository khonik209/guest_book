@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Гостевая книга</div>
                @include('common.error')
                @include('common.info')
                @include('common.success')

                <div class="panel-body">
                    <div id="info"></div>
                    @include('messages.form')
                    <hr>
                    @each('messages.message', $messages, 'message', 'messages.empty')
                    {{$messages->render()}}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
