/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');
require('../../../node_modules/jquery-validation/dist/jquery.validate.min');
window.Vue = require('vue');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('example-component', require('./components/ExampleComponent.vue'));

const app = new Vue({
    el: '#app'
});

$('#register-form').validate({
    submitHandler: function (form) {
        $.ajax({
            type: 'POST',
            url: '/validate/register',
            data: {
                'email': $('#email').val(),
                'name': $('#name').val(),
                'birthday': $("#birthday").val(),
                '_token': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (msg) {
                if (msg == '0') {
                    form.submit();
                } else {
                    $('.panel-body').append('<div class="alert alert-danger">' + msg + '</div>')
                }
            }
        });
    }
});

$('.form-validate').validate({
    submitHandler: function (form) {
        var form_data = new FormData();
        form_data.append("image", document.getElementById('image').files[0]);
        form_data.append('text', $('#text').val());
        form_data.append('_token', $('meta[name="csrf-token"]').attr('content'));
        $.ajax({
            type: 'POST',
            url: '/validate/message',
            dataType: 'text', // what to expect back from the PHP script
            cache: false,
            contentType: false,
            processData: false,
            data: form_data,
            success: function (msg) {
                if (msg == '0') {
                    form.submit();
                } else {
                    $('#info').append('<div class="alert alert-danger">' + msg + '</div>')
                }
            }
        });
    }
});

$('.comment-validate').validate();