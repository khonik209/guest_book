<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();
Route::post('/register','UserController@register');
Route::post('/login','UserController@login');
Route::get('/home', 'HomeController@index')->name('home');
Route::post('/message/new','HomeController@newMessage');
Route::post('/comment/new','HomeController@newComment');
Route::get('/message/{id}/edit','HomeController@messageEditPage');
Route::post('/message/edit','HomeController@messageEdit');
Route::post('/validate/register','UserController@validateRegisterForm');
Route::post('/validate/message','HomeController@validateMessageForm');
