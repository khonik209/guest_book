<?php

use Illuminate\Database\Seeder;

class ExampleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'user_1',
            'email' => 'user1@book.ru',
            'password' => bcrypt('123456'),
        ]);
        DB::table('users')->insert([
            'name' => 'user_2',
            'email' => 'user2@book.ru',
            'password' => bcrypt('123456'),
        ]);

        $users = \book\User::all();

        foreach ($users as $user) {
            $message = new \book\Message;
            $message->text = str_random(25);
            $message->user_id = $user->id;
            $message->save();

            $message2 = new \book\Message;
            $message2->text = str_random(25);
            $message2->user_id = $user->id;
            $message2->save();
        }

        $messages = \book\Message::all();
        foreach ($messages as $key => $message) {
            if ($key % 2 == 0) {
                $comment = new \book\Comment();
                $comment->text = str_random(10);
                $comment->message_id = $message->id;
                $comment->user_id = $message->user->id;
                $comment->save();

                $message->is_answered = 1;
                $message->save();

            }
        }
    }
}
